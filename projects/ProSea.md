#Управление и поиск проектов
## Автор
P3221 Во Минь Тхиен Лонг

P3221 Нгуен Зюи Кхань
## Описание
Prosea - это веб-сайт, который помогает человеку, который находит интересные и перспективные программированные проекты, из которых он может присоединиться к проекту или использовать его как полезный открытый исходный код.

Через него кандидаты могут связываться с владельцами бизнеса или знакомиться с другими программистами.
- Искать интересные проекты, реальные проекты
- Свяжите владельцев бизнеса и программистов

На странице проекты, у которых framework, язык программирования, краткое описание , требование, зарплата, работодатель, 
их связь с Github или Gitlab, но на нашей странице нет коды, …

## Детали
- Продукт - веб-приложение c базой данных, пользовательский интерфейс - веб-страницы.
- Back end: Java, Spring MVC;
- Database: JPA, NoSQL(json, xml).
- Front end: HTML5, CSS, Bootstrap and Javascript.
